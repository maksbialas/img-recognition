from PIL import Image

size = 32

for i in range(0,12499):
	im = Image.open("./train/cat." + str(i) + ".jpg")
	width, height = im.size
	if width > size and height > size:
		if height > width:
			top = (height - width)/2
			bottom = (height + width)/2
			im = im.crop((0, top, width, bottom))
		else:
			left = (width - height)/2
			right = (width + height)/2
			im = im.crop((left, 0, right, height))
		im = im.resize((size, size))
		im.save("./train_" + str(size) + "/cat." + str(i) + ".jpg")
	im = Image.open("./train/dog." + str(i) + ".jpg")
	width, height = im.size
	if width > size and height > size:
		if height > width:
			top = (height - width)/2
			bottom = (height + width)/2
			im = im.crop((0, top, width, bottom))
		else:
			left = (width - height)/2
			right = (width + height)/2
			im = im.crop((left, 0, right, height))
		im = im.resize((size, size))
		im.save("./train_" + str(size) + "/dog." + str(i) + ".jpg")
	im = Image.open("./test1/" + str(i+1) + ".jpg")
	width, height = im.size
	if width > size and height > size:
		if height > width:
			top = (height - width)/2
			bottom = (height + width)/2
			im = im.crop((0, top, width, bottom))
		else:
			left = (width - height)/2
			right = (width + height)/2
			im = im.crop((left, 0, right, height))
		im = im.resize((size, size))
		im.save("./test_" + str(size) + "/" + str(i) + ".jpg")


