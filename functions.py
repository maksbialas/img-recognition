import numpy as np
import copy


def sigmoid(z):
    s = 1 / (1 + np.exp(-z))
    cache = (z,)

    return s, cache


def relu(z):
    r = z * (z > 0)
    cache = (z,)

    return r, cache


def initialize_parameters(layer_dims):
    # initialize with He initialization
    parameters = {}
    # initalize parameters for every dimension contained in layer_dims
    for ll in range(1, len(layer_dims)):
        parameters["W" + str(ll)] = np.random.randn(layer_dims[ll], layer_dims[ll - 1]) * np.sqrt(2. / layer_dims[ll - 1])
        parameters["b" + str(ll)] = np.zeros((layer_dims[ll], 1))

    return parameters


def initialize_adam(layer_dims):
    v = {}
    s = {}

    for ll in range(1, len(layer_dims)):
        v["dW" + str(ll)] = np.zeros((layer_dims[ll], layer_dims[ll - 1]))
        v["db" + str(ll)] = np.zeros((layer_dims[ll], 1))
        s["dW" + str(ll)] = np.zeros((layer_dims[ll], layer_dims[ll - 1]))
        s["db" + str(ll)] = np.zeros((layer_dims[ll], 1))

    return v, s


def fw_linear(A, W, b):
    Z = np.dot(W, A) + b
    cache = (A, W, b)

    return Z, cache


def fw_activation(A_prev, W, b, act):
    # linear chache contains input of fw_linear
    Z, linear_cache = fw_linear(A_prev, W, b)
    if act == "sigmoid":
        A, activation_cache = sigmoid(Z)
    elif act == "relu":
        A, activation_cache = relu(Z)
    # activation chache contains input of activation function
    cache = (linear_cache, activation_cache)

    return A, cache


def fw_alllayers(X, parameters, keep_probs):
    A = X
    L = int(len(parameters) // 2)
    caches = []
    dropout_cache = {}

    for ll in range(1, L):
        prob = keep_probs[ll - 1]
        A_prev = A
        A, cache = fw_activation(A_prev, parameters["W" + str(ll)], parameters["b" + str(ll)], "relu")
        # dropout
        D_curr = np.random.rand(*A.shape)
        D_curr = (D_curr < prob).astype(int)
        A = A * D_curr
        A = A / prob
        dropout_cache["D" + str(ll)] = D_curr
        caches.append(cache)
    AL, cache = fw_activation(A, parameters["W" + str(L)], parameters["b" + str(L)], "sigmoid")
    caches.append(cache)

    return AL, caches, dropout_cache


def compute_cost(AL, Y, parameters, lmbd):
    # cost calculation
    m = Y.shape[1]
    L = int(len(parameters) // 2)
    # compute cost
    cost = -np.sum(np.multiply(Y, np.log(AL)) + np.multiply(1 - Y, np.log(1 - AL))) / m
    # add L2 term
    for ll in range(1, L + 1):
        cost = cost + np.sum(np.square(parameters["W" + str(ll)])) * lmbd / (2 * m)
    cost = np.squeeze(cost)

    return cost


def bw_linear(dZ, cache):
    A_prev, W, b = cache
    m = A_prev.shape[1]
    dW = np.dot(dZ, A_prev.T) / m
    db = np.sum(dZ, axis=1, keepdims=True) / m
    dA_prev = np.dot(W.T, dZ)

    return dA_prev, dW, db


def sigmoid_backward(dA, activation_cache):
    z = activation_cache[0]
    g = 1 / (1 + np.exp(-z))
    dZ = dA * g * (1 - g)

    return dZ


def relu_backward(dA, activation_cache):
    z = activation_cache[0]
    dZ = np.array(dA, copy=True)
    dZ[z <= 0] = 0

    return dZ


def bw_activation(dA, cache, act):
    linear_cache, activation_cache = cache

    if act == "sigmoid":
        dZ = sigmoid_backward(dA, activation_cache)
    elif act == "relu":
        dZ = relu_backward(dA, activation_cache)

    dA_prev, dW, db = bw_linear(dZ, linear_cache)

    return dA_prev, dW, db


def bw_alllayers(AL, Y, caches, keep_probs, dropout_cache):
    grads = {}
    L = len(caches)
    Y = Y.reshape(AL.shape)

    dAL = - (np.divide(Y, AL) - np.divide(1 - Y, 1 - AL))

    current_cache = caches[L - 1]
    grads["dA" + str(L - 1)], grads["dW" + str(L)], grads["db" + str(L)] = bw_activation(dAL, current_cache, "sigmoid")

    for ll in reversed(range(1, L - 1)):
        current_cache = caches[ll]
        dA_curr, grads["dW" + str(ll + 1)], grads["db" + str(ll + 1)] = bw_activation(grads["dA" + str(ll + 1)], current_cache, "relu")
        dA_curr * dropout_cache["D" + str(ll)] / keep_probs[ll - 1]
        grads["dA" + str(ll)] = dA_curr

    current_cache = caches[0]
    grads["dA0"], grads["dW1"], grads["db1"] = bw_activation(grads["dA1"], current_cache, "relu")

    return grads


def update_parameters(parameters, grads, learning_rate, lmbd, m):
    # update parameters w/ L2

    L = int(len(parameters) // 2)

    for ll in range(L):
        parameters["W" + str(ll + 1)] = parameters["W" + str(ll + 1)] - learning_rate * (grads["dW" + str(ll + 1)] + parameters["W" + str(ll + 1)] * lmbd / m)
        parameters["b" + str(ll + 1)] = parameters["b" + str(ll + 1)] - learning_rate * grads["db" + str(ll + 1)]

    return parameters


def update_parameters_adam(parameters, grads, v, s, t, learning_rate, beta1=0.9, beta2=0.999,  epsilon=1e-8):
    # update parameters w/ Adam optimization

    L = int(len(parameters) // 2)
    v_corrected = {}
    s_corrected = {}

    for ll in range(L):
        v["dW" + str(ll + 1)] = beta1 * v["dW" + str(ll + 1)] + (1 - beta1) * grads["dW" + str(ll + 1)]
        v["db" + str(ll + 1)] = beta1 * v["db" + str(ll + 1)] + (1 - beta1) * grads["db" + str(ll + 1)]

        s["dW" + str(ll + 1)] = beta2 * s["dW" + str(ll + 1)] + (1 - beta2) * np.square(grads["dW" + str(ll + 1)])
        s["db" + str(ll + 1)] = beta2 * s["db" + str(ll + 1)] + (1 - beta2) * np.square(grads["db" + str(ll + 1)])

        v_corrected["dW" + str(ll + 1)] = v["dW" + str(ll + 1)] / (1 - np.power(beta1, t))
        v_corrected["db" + str(ll + 1)] = v["db" + str(ll + 1)] / (1 - np.power(beta1, t))

        s_corrected["dW" + str(ll + 1)] = s["dW" + str(ll + 1)] / (1 - np.power(beta2, t))
        s_corrected["db" + str(ll + 1)] = s["db" + str(ll + 1)] / (1 - np.power(beta2, t))

        parameters["W" + str(ll + 1)] = parameters["W" + str(ll + 1)] - learning_rate * v_corrected["dW" + str(ll + 1)]/(np.sqrt(s_corrected["dW" + str(ll + 1)]) + epsilon)
        parameters["b" + str(ll + 1)] = parameters["b" + str(ll + 1)] - learning_rate * v_corrected["db" + str(ll + 1)]/(np.sqrt(s_corrected["db" + str(ll + 1)]) + epsilon)

    return parameters, v, s


def grad_to_vector(grads):
    L = int(len(grads) // 3)
    grads_v = []

    for ll in range(L):
        grads_v.extend(grads["dW" + str(ll + 1)].ravel())
        grads_v.extend(grads["db" + str(ll + 1)].ravel())

    grads_v = np.array(grads_v).reshape((len(grads_v), 1))

    return grads_v


def parameters_to_vector(parameters):
    L = int(len(parameters) // 2)
    parameters_v = []

    for ll in range(L):
        parameters_v.extend(parameters["W" + str(ll + 1)].ravel())
        parameters_v.extend(parameters["b" + str(ll + 1)].ravel())

    parameters_v = np.array(parameters_v).reshape((len(parameters_v), 1))

    return parameters_v


def vector_to_parameters(vector, parameters_og):
    L = int(len(parameters_og) // 2)
    parameters_result = {}
    last_elem = 0

    for ll in range(L):
        # fill for W
        shape = parameters_og["W" + str(ll + 1)].shape
        size = shape[0] * shape[1]

        parameters_result["W" + str(ll + 1)] = vector[last_elem:last_elem + size].reshape(*shape)
        last_elem = last_elem + size

        # fill for b
        shape = parameters_og["b" + str(ll + 1)].shape
        size = shape[0] * shape[1]

        parameters_result["b" + str(ll + 1)] = vector[last_elem:last_elem + size].reshape(*shape)
        last_elem = last_elem + size

    return parameters_result


def gradient_check(parameters, gradients, X, Y, lmbd, keep_probs, epsilon=1e-7):
    parameters_v = parameters_to_vector(parameters)
    grad = grad_to_vector(gradients)

    num_parameters = parameters_v.shape[0]
    gradapprox = np.zeros((num_parameters, 1))
    for ll in range(num_parameters):
        if ll % 1000 == 0:
            print("Gradient check:", str(100 * ll / num_parameters) + "%", "done")
        thetaplus = np.copy(parameters_v)
        thetaminus = np.copy(parameters_v)

        thetaplus[ll][0] = thetaplus[ll][0] + epsilon
        thetaminus[ll][0] = thetaminus[ll][0] - epsilon

        thetaplus = vector_to_parameters(thetaplus, parameters)
        thetaminus = vector_to_parameters(thetaminus, parameters)

        ALplus, _, _ = fw_alllayers(X, thetaplus, keep_probs)
        ALminus, _, _ = fw_alllayers(X, thetaminus, keep_probs)

        Jplus = compute_cost(ALplus, Y, thetaplus, lmbd)
        Jminus = compute_cost(ALminus, Y, thetaminus, lmbd)

        gradapprox[ll] = (Jplus - Jminus) / (2 * epsilon)

    numerator = np.linalg.norm(grad - gradapprox)
    denominator = np.linalg.norm(grad) + np.linalg.norm(gradapprox)
    difference = numerator / denominator

    if difference > 2e-7:
        print("\033[93m" + "There is a mistake in the backward propagation! difference = " + str(difference) + "\033[0m")
    else:
        print("\033[92m" + "Your backward propagation works perfectly fine! difference = " + str(difference) + "\033[0m")

    return difference
