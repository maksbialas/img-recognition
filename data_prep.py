import numpy as np

def divide_to_batches(train_set, labels, batch_size):
	m_train = np.size(train_set, axis=1)
	batches_X = []
	batches_Y = []

	for ii in range(int(m_train // batch_size)):
	    batches_X.append(train_set[:, ii * batch_size:(ii + 1) * batch_size])
	    batches_Y.append(labels[:, ii * batch_size:(ii + 1) * batch_size])
	batches_X.append(train_set[:, int(m_train // batch_size):])
	batches_Y.append(labels[:, int(m_train // batch_size):])

	return batches_X, batches_Y


def shuffle(train_set, labels):
	num_px = np.size(train_set, axis=0)
	shufflable = np.vstack((train_set, labels)).T
	np.random.shuffle(shufflable)
	shufflable = shufflable.T

	train_set_res = shufflable[:num_px]
	labels_res = shufflable[num_px:]

	return train_set_res, labels_res
