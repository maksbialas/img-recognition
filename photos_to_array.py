from pathlib import Path
import numpy as np
import imageio

train_set = []
test_set = []
labels = []

# load cat photos
for i in range(0, 12499):
    file_path = Path("./train_32/cat." + str(i) + ".jpg")
    if file_path.is_file():
        im = imageio.imread(file_path)
        train_set.append(im.flatten())
        labels.append(0)
print("Cat photos loaded")

# load dog photos
for i in range(0, 12499):
    file_path = Path("./train_32/dog." + str(i) + ".jpg")
    if file_path.is_file():
        im = imageio.imread(file_path)
        train_set.append(im.flatten())
        labels.append(1)
print("Dog photos loaded")

train_set = np.array(train_set).T/255
np.save('./arrays/train_set_32.npy', train_set)
print("Train set saved")

labels = np.array(labels).reshape((1, len(labels)))
np.save('./arrays/labels_32.npy', labels)
print("Labels set saved")

# load test photos
for i in range(0, 12498):
    file_path = Path("./test_32/" + str(i) + ".jpg")
    if file_path.is_file():
        im = imageio.imread(file_path)
        test_set.append(im.flatten())

print("Test photos loaded")

test_set = np.array(test_set).T/255
np.save('./arrays/test_set_32.npy', test_set)
print("Test set saved")

