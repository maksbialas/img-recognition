import numpy as np
import functions as fn
import data_prep as dp
import matplotlib.pyplot as plt


train_range = np.r_[0:5000, 12498:17498]
batch_size = 500

# load sets from saved np.arrays
train_examples = np.load('./arrays/train_set_32.npy')
train_set = train_examples[:, train_range]
test_set = train_examples[:, np.r_[12000:12300, 24000:24300]]
labels = np.load('./arrays/labels_32.npy')[:, train_range]

train_set, labels = dp.shuffle(train_set, labels)


m_train = np.size(train_set, axis=1)
m_test = np.size(test_set, axis=1)
num_px = np.size(train_set, axis=0)

batches_X, batches_Y = dp.divide_to_batches(train_set, labels, batch_size)

print("Number of train examples:", m_train)
print("Batch size:", batch_size)
print("Number of test examples:", m_test)
print("Number of pixel values:", num_px)


# initalize Ws and bs
layer_dims = [num_px, 20, 11, 7, 1]
parameters = fn.initialize_parameters(layer_dims)
v, s = fn.initialize_adam(layer_dims)

rate = 0.001
lmbd = 0
keep_probs = [1, 1, 1]

print("Learning rate:", rate)

costs = []
grads = {}

print("Optimization started")
for ii in range(0, 3000):
    for jj in range(len(batches_X)):
        # forward propagation
        AL, caches, dropout_cache = fn.fw_alllayers(batches_X[jj], parameters, keep_probs)
        # cost computation
        if ii % 100 == 0:
            cost = fn.compute_cost(AL, batches_Y[jj], parameters, lmbd)
            print("Cost after iteration", str(ii) + ":", cost)
            costs.append(cost)
        # backward propagation
        grads = fn.bw_alllayers(AL, batches_Y[jj], caches, keep_probs, dropout_cache)
        # check = fn.gradient_check(parameters, grads, batches_X[jj], batches_Y[jj], lmbd, keep_probs)
        # parameters = fn.update_parameters(parameters, grads, rate, lmbd, m_train)
        parameters, v, s = fn.update_parameters_adam(parameters, grads, v, s, 2, rate)

correct_counter_train = 0
correct_counter_test = 0

guesses_train = fn.fw_alllayers(train_set, parameters, [1., 1., 1.])[0]
guesses_train = np.where(guesses_train < 0.5, 0, 1)
guesses_train = np.where(guesses_train == labels, 1, 0)

cat_guesses_test = fn.fw_alllayers(test_set[:, 0:300], parameters, [1., 1., 1.])[0]
dog_guesses_test = fn.fw_alllayers(test_set[:, 300:600], parameters, [1., 1., 1.])[0]

cat_guesses_test = np.where(cat_guesses_test < 0.5, 1, 0)
dog_guesses_test = np.where(dog_guesses_test > 0.5, 1, 0)

correct_counter_test = np.sum(cat_guesses_test) + np.sum(dog_guesses_test)
correct_counter_train = np.sum(guesses_train)

plt.plot(costs)


print("Correct train guesses percent:", correct_counter_train / m_train)
print("Correct test guesses percent:", correct_counter_test / m_test)
plt.show()
